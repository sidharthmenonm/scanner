import jQuery from "jquery";
import popper from "popper.js";
import bootstrap from "bootstrap";

window.$ = window.jQuery = jQuery;
window.bootstrap = bootstrap;
window.popper = popper;


import Vue from 'vue'
import VueQuagga from 'vue-quaggajs';
import App from './vue/app.vue';
Vue.use(VueQuagga);

const app = new Vue({
  el: "#app",
  render: h => h(App)
})
